package actors

import javax.inject.{Inject, Named, Singleton}

import actors.TradeCollectorActor.Collect
import actors.wex.BtcUsdTradeActor.{TradeCollect => UsdTradeCollect}
import actors.wex.BtcRurTradeActor.{TradeCollect => RurTradeCollect}
import akka.actor.{Actor, ActorRef}
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object TradeCollectorActor {

  case object Collect

}

@Singleton
class TradeCollectorActor @Inject()(
  @Named("btc-usd-trade-actor") val btcUsdTradeActor: ActorRef,
  @Named("btc-rur-trade-actor") val btcRurTradeActor: ActorRef
)(implicit ec: ExecutionContext) extends Actor {

  context.system.scheduler.schedule(10.seconds, 1.minute, self, Collect)

  private def collect(): Future[Boolean] = {
    btcUsdTradeActor ! UsdTradeCollect
    btcRurTradeActor ! RurTradeCollect
    Future(true)
  }

  override def receive = {
    case Collect => collect
  }
}
