package actors

import javax.inject.{Inject, Singleton}

import akka.actor.Actor

import scala.concurrent.{ExecutionContext, Future}
import actors.TelegramBotActor.Run
import telegram.Bot

import scala.concurrent.duration._
import logging.Log.app
import play.api.Environment

object TelegramBotActor {

  case object Run

}

@Singleton
class TelegramBotActor @Inject()(
  environment: Environment,
  bot: Bot
)(implicit ec: ExecutionContext) extends Actor {

  context.system.scheduler.scheduleOnce(5.seconds, self, Run)

  private def run: Future[Boolean] = {
    Future {
      app.info(s"Starting telegram bot aka ${bot.name} ")
      bot.run()
      true
    }
  }

  override def receive = {
    case Run => run
  }
}
