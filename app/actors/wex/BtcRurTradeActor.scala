package actors.wex

import actors.wex.BtcRurTradeActor.TradeCollect
import akka.actor.Actor
import javax.inject.{Inject, Singleton}

import services.WexService

import scala.concurrent.{ExecutionContext, Future}
import logging.Log.collector
import models.wex.BtcRur
import services.wex.BtcRurTradeService

object BtcRurTradeActor {

  case object TradeCollect

}

@Singleton
class BtcRurTradeActor @Inject()(
  tradeService: BtcRurTradeService,
  wexService: WexService
)(implicit ec: ExecutionContext) extends Actor {

  def tradeCollect: Future[Boolean] = {
    wexService.trades(BtcRur).flatMap { optionT => {
      optionT.map(tradesList => {
        collector.debug(s"Get  ${tradesList.length} RUR trades")
        val ids = tradeService.insertOnlyNew(tradesList)
        collector.debug(s"Save to DB  ${ids.length} RUR trades")
        Future(true)
      }).getOrElse(Future(false))
    }
    } recover {
      case e => {
        collector.error(e.getLocalizedMessage)
        Future(false)
      }
    }
    Future(true)
  }

  override def receive = {
    case TradeCollect => tradeCollect
  }
}
