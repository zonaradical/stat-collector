package actors.wex

import actors.wex.BtcRurTickerActor.TickerCollect
import akka.actor.Actor
import javax.inject.{Inject, Singleton}

import models.Ticker
import services.WexService
import services.wex.BtcRurTickerService

import scala.concurrent.{ExecutionContext, Future}
import logging.Log.collector
import models.wex.BtcRur

object BtcRurTickerActor {

  case object TickerCollect

}

@Singleton
class BtcRurTickerActor @Inject()(
  tickerService: BtcRurTickerService,
  wexService: WexService
)(implicit ec: ExecutionContext) extends Actor {

  def tickerCollect: Future[Boolean] = {
    wexService.tickers(BtcRur).flatMap {
      optionT => {
        optionT.map(
          ticker => {
            tickerService.save(ticker)
            collector.debug("Ticker BtcRUR: " + ticker.last)
            Future(true)
          }
        ).getOrElse(Future(false))
      }
    } recover {
      case e => {
        collector.error(e.getLocalizedMessage)
        Future(false)
      }
    }
    Future(true)
  }

  override def receive = {
    case TickerCollect => tickerCollect
  }
}
