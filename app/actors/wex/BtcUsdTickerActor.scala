package actors.wex

import actors.wex.BtcUsdTickerActor.TickerCollect
import akka.actor.Actor
import javax.inject.{Inject, Singleton}

import models.Ticker
import services.WexService
import services.wex.BtcUsdTickerService
import logging.Log.collector
import models.wex.BtcUsd

import scala.concurrent.{ExecutionContext, Future}

object BtcUsdTickerActor {

  case object TickerCollect

}

@Singleton
class BtcUsdTickerActor @Inject()(
  tickerService: BtcUsdTickerService,
  wexService: WexService
)(implicit ec: ExecutionContext) extends Actor {

  def tickerCollect: Future[Boolean] = {
    wexService.tickers(BtcUsd).flatMap {
      optionT => {
        optionT.map(
          ticker => {
            collector.debug("Ticker BtcUSD: " + ticker.last)
            tickerService.save(ticker)
            Future(true)
          }).getOrElse(Future(false))
      }
    } recover {
      case e => {
        collector.error(e.getLocalizedMessage)
        Future(false)
      }
    }
    Future(true)
  }

  override def receive = {
    case TickerCollect => tickerCollect
  }
}
