package actors.analytics


import javax.inject.Inject
import actors.analytics.AveragesActor.Calculate
import akka.actor.Actor
import info.mukel.telegrambot4s.methods.ParseMode
import org.joda.time.{DateTime, Interval}
import services.wex.{BtcRurTickerService, BtcUsdTickerService}
import telegram.Bot
import logging.Log.app
import play.api.Configuration

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

object AveragesActor {

  case object Calculate

}

class AveragesActor @Inject()(
  bot: Bot,
  btcUsdTickerService: BtcUsdTickerService,
  btcRurTickerService: BtcRurTickerService,
  config: Configuration
)(implicit ec: ExecutionContext) extends Actor {

  context.system.scheduler.schedule(15.seconds, 1.minute, self, Calculate)

  var HISTORY_USD_AVERAGE_DIFF = 0F
  var HISTORY_RUR_AVERAGE_DIFF = 0F

  private def calculateRUR(longInterval: Interval, smallInterval: Interval) = {
    val btcRurAverageLong = btcRurTickerService.averageForInterval(longInterval)
    app.debug("btcRurAverageLong = " + btcRurAverageLong)
    val btcRurAverageSmall = btcRurTickerService.averageForInterval(smallInterval)
    app.debug("btcRurAverageSmall = " + btcRurAverageSmall)
    val currentRurDiff = btcRurAverageLong - btcRurAverageSmall
    app.debug("currentRurDiff = " + currentRurDiff)
    val message =
      s"""
         |RUR Разность средних поменяла знак(или равна 0):
         |текущая разность `${currentRurDiff}`
         |предыдущая `${HISTORY_RUR_AVERAGE_DIFF}`
         |длинный интервал ${config.get[Int]("average.interval.long")} дней
         |короткий интервал ${config.get[Int]("average.interval.short")} час
      """.stripMargin

    if (Math.signum(currentRurDiff) == 0 |
      Math.signum(currentRurDiff) != Math.signum(HISTORY_RUR_AVERAGE_DIFF)) {
      bot.send(message, ParseMode.Markdown)
    }
    HISTORY_RUR_AVERAGE_DIFF = currentRurDiff
  }

  private def calculateUSD(longInterval: Interval, smallInterval: Interval) = {
    val btcUsdAverageLong = btcUsdTickerService.averageForInterval(longInterval)
    app.debug("btcUsdAverageLong = " + btcUsdAverageLong)
    val btcUsdAverageSmall = btcUsdTickerService.averageForInterval(smallInterval)
    app.debug("btcUsdAverageSmall = " + btcUsdAverageSmall)
    val currentUsdDiff = btcUsdAverageLong - btcUsdAverageSmall
    app.debug("currentUsdDiff = " + currentUsdDiff)
    val message =
      s"""
         |USD Разность средних поменяла знак(или равна 0):
         |текущая разность `${currentUsdDiff}`
         |предыдущая `${HISTORY_USD_AVERAGE_DIFF}`
         |длинный интервал ${config.get[Int]("average.interval.long")} дней
         |короткий интервал ${config.get[Int]("average.interval.short")} час
      """.stripMargin

    if (Math.signum(currentUsdDiff) == 0 |
      Math.signum(currentUsdDiff) != Math.signum(HISTORY_USD_AVERAGE_DIFF)) {
      bot.send(message, ParseMode.Markdown)
    }
    HISTORY_USD_AVERAGE_DIFF = currentUsdDiff
  }

  private def calculate: Unit = {
    val now = DateTime.now
    val longInterval = new Interval(now.minusDays(config.get[Int]("average.interval.long")), now)
    val smallInterval = new Interval(now.minusHours(config.get[Int]("average.interval.short")), now)
    calculateUSD(longInterval, smallInterval)
    calculateRUR(longInterval, smallInterval)
  }

  override def receive = {
    case Calculate => calculate
  }
}
