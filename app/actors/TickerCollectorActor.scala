package actors

import javax.inject.{Inject, Named, Singleton}

import actors.TradeCollectorActor.Collect
import actors.wex.BtcUsdTickerActor.{TickerCollect => UsdTickerCollect}
import actors.wex.BtcRurTickerActor.{TickerCollect => RurTickerCollect}
import akka.actor.{Actor, ActorRef}
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object TickerCollectorActor {

  case object Collect

}

@Singleton
class TickerCollectorActor @Inject()(
  @Named("btc-usd-ticker-actor") val btcUsdTickerActor: ActorRef,
  @Named("btc-rur-ticker-actor") val btcRurTickerActor: ActorRef
)(implicit ec: ExecutionContext) extends Actor {

  context.system.scheduler.schedule(10.seconds, 2.seconds, self, Collect)

  private def collect(): Future[Boolean] = {
    btcUsdTickerActor ! UsdTickerCollect
    btcRurTickerActor ! RurTickerCollect
    Future(true)
  }

  override def receive = {
    case Collect => collect
  }
}
