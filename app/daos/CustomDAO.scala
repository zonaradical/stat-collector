package daos

import com.mongodb.casbah.commons.MongoDBObject
import org.bson.types.ObjectId
import ru.tochkak.plugin.salat.PlaySalat
import salat.dao.{ModelCompanion, SalatDAO}

class CustomDAO[T <: AnyRef](
  collection: String,
  base: String
)(implicit mongoContext: MongoContext, playSalat: PlaySalat, m: Manifest[T]) extends ModelCompanion[T, ObjectId] {
  import mongoContext._

  val dao = new SalatDAO[T, ObjectId](playSalat.collection(collection, base)) {}

  def all(): List[T] = dao.find(MongoDBObject.empty).toList

}
