package daos.analytics

import javax.inject._

import com.mongodb.casbah.commons.MongoDBObject
import daos.{CustomDAO, MongoContext}
import models.Ticker
import org.joda.time.Interval
import ru.tochkak.plugin.salat.PlaySalat

class AverageDAO @Inject()(
  implicit val playSalat: PlaySalat,
  implicit val mongoContext: MongoContext
) extends CustomDAO[Ticker]("analytics_average", "wex") {
  def findByInterval(interval: Interval,skip: Int, limit: Int) = dao.find(
    MongoDBObject("updated" -> MongoDBObject("$gte" -> interval.getStart, "$lt" -> interval.getEnd))
  ).sort(
    MongoDBObject("updated" -> 1)
  ).skip(skip).limit(limit).toList

  def getCountByInterval(interval: Interval): Long = dao.count(
    MongoDBObject("updated" -> MongoDBObject("$gte" -> interval.getStart, "$lt" -> interval.getEnd))
  )
}

