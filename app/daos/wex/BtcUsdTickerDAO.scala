package daos.wex

import javax.inject._

import com.mongodb.casbah.commons.MongoDBObject
import daos.{CustomDAO, MongoContext}
import models.Ticker
import org.joda.time.Interval
import ru.tochkak.plugin.salat.PlaySalat

class BtcUsdTickerDAO @Inject()(
  implicit val playSalat: PlaySalat,
  implicit val mongoContext: MongoContext
) extends CustomDAO[Ticker]("ticker_btc_usd", "wex") {

  def findByIntervalWithPagination(interval: Interval, offset: Int, limit: Int): List[Ticker] = dao.find(
    MongoDBObject("updated" -> MongoDBObject("$gte" -> interval.getStart, "$lt" -> interval.getEnd))
  ).sort(
    MongoDBObject("updated" -> 1)
  ).skip(offset)
    .limit(limit)
    .toList

  def countByInterval(interval: Interval): Long = dao.count(
    MongoDBObject("updated" -> MongoDBObject("$gte" -> interval.getStart, "$lt" -> interval.getEnd))
  )
}
