package daos.wex

import javax.inject._

import com.mongodb.casbah.commons.MongoDBObject
import daos.{CustomDAO, MongoContext}
import models.Trade
import ru.tochkak.plugin.salat.PlaySalat

class BtcRurTradeDAO @Inject()(
  implicit val playSalat: PlaySalat,
  implicit val mongoContext: MongoContext
) extends CustomDAO[Trade]("trade_btc_rur", "wex") {
  def findByTid(tid: Int): Option[Trade] = {
    dao.findOne(
      MongoDBObject(
        "tid" -> tid
      )
    )
  }

  def findLast: Option[Trade] = dao.find(
    MongoDBObject(
      "type" -> MongoDBObject("$exists" -> true)
    )
  ).sort(
    MongoDBObject(
      "timestamp" -> -1
    )
  ).limit(1).toList.headOption
}
