package telegram

import javax.inject.{Inject, Singleton}

import info.mukel.telegrambot4s.api.declarative.Commands
import info.mukel.telegrambot4s.api.{Polling, TelegramBot}
import info.mukel.telegrambot4s.methods.ParseMode.ParseMode
import info.mukel.telegrambot4s.methods.{ParseMode, SendMessage}
import info.mukel.telegrambot4s.models.ChatId
import play.api.Configuration
import services.WexService
import logging.Log.app
import models.wex.{BtcRur, BtcUsd}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.postfixOps
import scala.concurrent.duration._


@Singleton
class Bot @Inject()(
  config: Configuration,
  wexService: WexService
)(implicit ec: ExecutionContext) extends TelegramBot
  with Polling
  with Commands {

  lazy val token = config.get[String]("telegram.bot.token")
  val name = config.get[String]("telegram.bot.name")

  def send(text: String, parseMode: ParseMode) = {
    val to = config.get[Int]("telegram.bot.chat_id")
    request(
      SendMessage(
        ChatId(to), text, Some(parseMode)
      )
    )
  }

  onCommand('hello) { implicit msg => reply("Привет, ребятня! Ваш ChatId = " + msg.source) }

  onCommand('usd) { implicit msg =>
    wexService.tickers(BtcUsd).flatMap { optionT => {
      optionT.map(ticker => {
        val f = Future(
          reply({
            "Последняя сделка `" + ticker.last.toString + "`"
          }, Some(ParseMode.Markdown))
        )
        Await.result(f, 10 second)
      }).getOrElse(
        Future(
          reply({
            "Последняя сделка `unknown`"
          }, Some(ParseMode.Markdown))
        )
      )
    }
    } recover {
      case e => {
        app.error("Failed to get ticker: " + e.getLocalizedMessage)
        reply({
          "Последняя сделка `unknown`"
        }, Some(ParseMode.Markdown))
      }
    }
  }

  onCommand('rur) { implicit msg =>
    wexService.tickers(BtcRur).flatMap { optionT => {
      optionT.map(ticker => {
        Future(
          reply({
            "Последняя сделка `" + ticker.last.toString + "`"
          }, Some(ParseMode.Markdown))
        )
      }).getOrElse(
        Future(
          reply({
            "Последняя сделка `unknown`"
          }, Some(ParseMode.Markdown))
        )
      )
    }
    } recover {
      case e => {
        app.error("Failed to get ticker: " + e.getLocalizedMessage)
        reply({
          "Последняя сделка `unknown`"
        }, Some(ParseMode.Markdown))
      }
    }
  }
}

case object Bot
