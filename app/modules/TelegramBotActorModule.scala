package modules

import actors.TelegramBotActor
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class TelegramBotActorModule extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bindActor[TelegramBotActor]("telegram-bot-actor")
  }
}
