package modules

import actors.analytics.AveragesActor
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class AnalyticsActorModule extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bindActor[AveragesActor]("averages-actor")
  }
}
