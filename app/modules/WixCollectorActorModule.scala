package modules

import actors.{TickerCollectorActor, TradeCollectorActor}
import actors.wex.{BtcRurTickerActor, BtcRurTradeActor, BtcUsdTickerActor, BtcUsdTradeActor}
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class WixCollectorActorModule extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bindActor[TradeCollectorActor]("trade-collector-actor")
    bindActor[TickerCollectorActor]("ticker-collector-actor")
    bindActor[BtcUsdTradeActor]("btc-usd-trade-actor")
    bindActor[BtcRurTradeActor]("btc-rur-trade-actor")
    bindActor[BtcUsdTickerActor]("btc-usd-ticker-actor")
    bindActor[BtcRurTickerActor]("btc-rur-ticker-actor")
  }
}
