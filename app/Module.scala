import com.google.inject.AbstractModule
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers

class Module extends AbstractModule {

  override def configure() = {
    RegisterJodaTimeConversionHelpers()
  }

}
