package controllers

import javax.inject.{Inject, Named}

import akka.actor.ActorRef
import org.joda.time.{DateTime, Interval}
import play.api.Environment
import play.api.libs.ws.WSClient
import play.api.mvc.InjectedController

import scala.concurrent.ExecutionContext
import services.WexService
import services.wex.{BtcUsdTickerService, BtcUsdTradeService}
import telegram.Bot

class ExampleController @Inject()(
  bot: Bot,
  wexService: WexService,
  ws: WSClient,
  tradeService: BtcUsdTradeService,
  btcUsdTickerService: BtcUsdTickerService,
  environment: Environment
)(implicit ec: ExecutionContext) extends InjectedController {
  def index = Action {
    request => {
      val now = DateTime.now
      val interval = new Interval(now.minusMonths(1) , now)
      logging.Log.app.info("Average USD for last hour: " + btcUsdTickerService.averageForInterval(interval))
      Ok("Environment " + environment.mode)
    }
  }
}
