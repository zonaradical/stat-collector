package logging

import play.api.Logger

object Log {
  lazy val app = Logger("application")
  lazy val collector = Logger("collector_actor")
}