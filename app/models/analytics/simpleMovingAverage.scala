package models.analytics

import models.wex.Couple
import org.bson.types.ObjectId
import org.joda.time.DateTime

case class simpleMovingAverage(
  _id: ObjectId,
  couple: Couple,
  value: Double,
  updated: DateTime = DateTime.now
)
