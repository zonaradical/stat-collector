package models

import org.joda.time.DateTime
import play.api.libs.json.Reads
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.json.Reads._

case class Ticker(
  high: Float,
  low: Float,
  avg: Float,
  vol: Float,
  vol_cur: Float,
  last: Float,
  buy: Float,
  sell: Float,
  updated: DateTime
)

trait TickerJson {
  def reads: Reads[Ticker] = {
    (
      (__ \ "high").read[Float] and
        (__ \ "low").read[Float] and
        (__ \ "avg").read[Float] and
        (__ \ "vol").read[Float] and
        (__ \ "vol_cur").read[Float] and
        (__ \ "last").read[Float] and
        (__ \ "buy").read[Float] and
        (__ \ "sell").read[Float] and
        (__ \ "updated").read[Int].map(unix => new DateTime(unix * 1000L))
      ) (Ticker.apply _)
  }
}

object Ticker extends TickerJson {

  val URL_TRADES = "https://wex.nz/api/3/trades/"
  val URL_DEPTH = "https://wex.nz/api/3/depth/"
  val URL_TICKER = "https://wex.nz/api/3/ticker/"

  val PAIR_BTC_USD = "btc_usd"
  val PAIR_BTC_RUR = "btc_rur"
  val DEPTH_LIMIT = "5000"
  val ASKS = "ask"
  val BIDS = "bid"
}