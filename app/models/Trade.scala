package models

import org.joda.time.DateTime
import play.api.libs.json.Reads
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.json.Reads._

case class Trade(
  `type`: String,
  price: Float,
  amount: Float,
  tid: Int,
  timestamp: DateTime
)

trait TradeJson {
  def reads: Reads[Trade] = {
    (
      (__ \ "type").read[String] and
        (__ \ "price").read[Float] and
        (__ \ "amount").read[Float] and
        (__ \ "tid").read[Int] and
        (__ \ "timestamp").read[Int].map(unix => new DateTime(unix * 1000L))
      ) (Trade.apply _)
  }
}

object Trade extends TradeJson {

  val URL_TRADES = "https://wex.nz/api/3/trades/"

  val DEPTH_LIMIT = "500"
}