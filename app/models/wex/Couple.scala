package models.wex

sealed trait Couple {
  def name: String
}

case object BtcUsd extends Couple {
  val name = "btc_usd"
}

case object BtcRur extends Couple {
  val name = "btc_rur"
}