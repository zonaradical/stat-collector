package services.wex

import javax.inject.Inject

import com.mongodb.WriteConcern
import daos.wex.BtcUsdTradeDAO
import models.Trade
import org.joda.time.DateTime

class BtcUsdTradeService @Inject()(
  btcUsdTradeDAO: BtcUsdTradeDAO
) {

  def insertOnlyNew(tradeList: List[Trade]) = {
    val lastTimestamp: DateTime = btcUsdTradeDAO.findLast.map(trade => trade.timestamp).getOrElse(new DateTime(0))
    val filteredList: List[Trade] = tradeList.filter(
      trade => trade.timestamp isAfter (lastTimestamp)
    )
    btcUsdTradeDAO.insert(filteredList, WriteConcern.W3)
  }
}
