package services.wex

import javax.inject.Inject

import daos.wex.BtcUsdTickerDAO
import models.Ticker
import org.joda.time.Interval
import play.api.Configuration

class BtcUsdTickerService @Inject()(
  btcUsdTickerDAO: BtcUsdTickerDAO,
  config: Configuration
) {

  def save(ticker: Ticker) = btcUsdTickerDAO.save(ticker)

  def averageForInterval(inteval: Interval) = {
    val pageSize = config.get[Int]("average.page.size")
    val count = countByInterval(inteval)
    val numberOfPages = count / pageSize

    (0 to numberOfPages.toInt).map {
      page => {
        val offset = page * pageSize
        val limit = if ((page + 1) * pageSize > count) count.toInt - offset else pageSize
        btcUsdTickerDAO.findByIntervalWithPagination(inteval, offset, limit).foldLeft(0F)(_ + _.last) / count
      }
    }.sum
  }

  def countByInterval(interval: Interval): Long = btcUsdTickerDAO.countByInterval(interval)

}
