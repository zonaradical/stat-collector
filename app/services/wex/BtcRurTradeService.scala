package services.wex

import javax.inject.Inject

import com.mongodb.WriteConcern
import daos.wex.BtcRurTradeDAO
import models.Trade
import org.joda.time.DateTime

class BtcRurTradeService @Inject()(
  btcRurTradeDAO: BtcRurTradeDAO
) {

  def insertOnlyNew(tradeList: List[Trade]) = {
    val lastTimestamp: DateTime = btcRurTradeDAO.findLast.map(trade => trade.timestamp).getOrElse(new DateTime(0))
    val filteredList: List[Trade] = tradeList.filter(
      trade => trade.timestamp isAfter (lastTimestamp)
    )
    btcRurTradeDAO.insert(filteredList, WriteConcern.W3)
  }

}
