package services

import javax.inject.Inject

import logging.Log.collector
import models.wex.Couple
import models.{Ticker, Trade}
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, Reads}
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContext, Future}


class WexService @Inject()(
  config: Configuration,
  ws: WSClient
)(implicit ec: ExecutionContext) {
  def trades(couple: Couple): Future[Option[List[Trade]]] = {
    ws.
      url(Trade.URL_TRADES + couple.name).
      addQueryStringParameters("limit" -> config.get[Int]("wex.trade.depth_limit").toString).get.map(
      response => {
        (response.json \ couple.name).validate(Reads.list[Trade](Trade.reads)) match {
          case tradesList: JsSuccess[List[Trade]] => Some(tradesList.get)
          case error: JsError => {
            collector.error(JsError.toJson(error).toString())
            None
          }
        }
      }
    )
  }

  def tickers(couple: Couple): Future[Option[Ticker]] = {
    ws.
      url(Ticker.URL_TICKER + couple.name).
      get.map(response => {
      (response.json \ couple.name).validate(Ticker.reads) match {
        case ticker: JsSuccess[Ticker] => Some(ticker.get)
        case error: JsError => {
          collector.error(JsError.toJson(error).toString())
          None
        }
      }
    })
  }
}
