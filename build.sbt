name := "service"
organization := "ru.kinoplan24"
version := "0.1"

scalaVersion := "2.12.4"

lazy val root = project.in(file(".")).enablePlugins(PlayScala)

libraryDependencies += guice
libraryDependencies ++= Seq(
  guice,
  "ru.tochkak" %% "play-plugins-salat" % "1.7.2",
  ws,
  "com.typesafe.play"       %% "play-json-joda"     % "2.6.6",
  "org.joda" % "joda-convert" % "1.9.2",
  "info.mukel" %% "telegrambot4s" % "3.0.14",
  specs2 % Test
)

scalacOptions += "-deprecation"
scalacOptions += "-feature"

fork            in run := true
sources         in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false
packageName     in Universal := "service"
